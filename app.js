// Open all commentary tabs
document
  .querySelectorAll(".q-question-belt li:nth-child(2) a")
  .forEach((e) => e.click());

let PAGE = "";

const cards = document.querySelectorAll(".q-question-body");
const enuciationArray = document.querySelectorAll(
  ".q-question-body .q-question-enunciation"
);
const commentsList = document.querySelectorAll(".js-question-comments-list");

for (let i = 0; i < cards.length; i++) {
  const questionHeader =
    cards[i].previousElementSibling.previousElementSibling
      .previousElementSibling.textContent;
  const questionInfo =
    cards[i].previousElementSibling.previousElementSibling.textContent;
  const extraText = cards[i].querySelector(".q-link + div");
  const enunciation =
    "<strong>" + enuciationArray[i].innerText + "</strong>" + "<br><br>";
  const alternatives = Array.from(
    cards[i].querySelectorAll(".q-question-options div")
  );

  // const alternativesArray = alternatives.flatMap((e) =>
  //   e.innerText.includes("\n") ? [formatAlternative(e.innerText)] : []
  // );

  const alternativesArray = alternatives.reduce((result, alt, index) => {
    if (index % 2 === 0) {
      result.push(formatAlternative(alt.innerText));
    }
    return result;
  }, []);

  const alternativesFormatted = `<div class="alt-container">${alternativesArray.join(
    "<br><br>"
  )}</div>`;

  const commentsArray = Array.from(
    commentsList[i].querySelectorAll(
      ".js-question-comment .js-question-comment-text"
    )
  );

  const comments = commentsArray
    .map((e) => formartComment(e.innerText))
    .join("");

  let extra = "";
  if (extraText)
    extra =
      "<a class='extra'><span>Texto associado</span></a>" +
      "<div class='extra-text'>" +
      extraText.innerHTML +
      "</div>";

  PAGE += createCard(
    questionHeader,
    questionInfo,
    extra,
    enunciation,
    alternativesFormatted,
    comments
  );
}

function formartComment(comment) {
  return "<p>" + comment + "</p>";
}

function formatAlternative(string) {
  if (string == "Certo" || string == "Errado")
    return "<strong>" + string + "</strong>";
  string = string.replace("\n", " - ");
  const letter = "<strong>" + string.charAt(0) + ")" + "</strong>   ";
  return string.substring(0, 0) + letter + string.substring(0 + 1);
}

function createCard(
  questionHeader,
  questionInfo,
  extra = "",
  enunciation,
  alternatives,
  comments
) {
  return (
    "<div class='question-header'>" +
    questionHeader +
    "</div>" +
    "<div class='question-info'>" +
    questionInfo +
    "</div" +
    '<div class="card-body">' +
    extra +
    enunciation +
    alternatives +
    '<div class="accordion">' +
    '<div class="comment-body">' +
    '<div class="comment-belt">Comentários' +
    '<div class="comment-list">' +
    comments +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<br><br><br>"
  );
}

const SERVER_PORT = 8080;
const SERVER_PATH = "source_files";
const STYLE = `<link rel="stylesheet" href="http://127.0.0.1:${SERVER_PORT}/${SERVER_PATH}/styles.css" />`;
const SCRIPT = `<script src="http://127.0.0.1:${SERVER_PORT}/${SERVER_PATH}/script.js" defer></script>`;

const CONTENT = `<!DOCTYPE html> <html lang="en"> <head>${STYLE} ${SCRIPT}</head> <body>${PAGE}</body> </html>`;
// const tab = window.open();
// tab.document.write(content);

const encoded = btoa(unescape(encodeURI(CONTENT)));
fetch("http://127.0.0.1:9922", {
  method: "POST",
  body: encoded,
});
