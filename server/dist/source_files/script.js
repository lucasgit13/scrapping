document.querySelectorAll(".comment-belt").forEach((belt) => {
  belt.addEventListener("click", () => {
    const list = belt.children[0];
    list.classList.toggle("active");
    belt.classList.toggle("active");
    // console.log('test')
  });
});

document.querySelectorAll(".p-alt").forEach((p) => {
  p.addEventListener("click", () => {
    p.classList.toggle("active");
  });
});

document.querySelectorAll(".extra").forEach((a) => {
  a.addEventListener("click", () => {
    const text = a.nextElementSibling;
    text.classList.toggle("active");
  });
});
